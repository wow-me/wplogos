require('../../libs/slick/slick.js');
require('../../libs/color-picker/color-picker.js');
require('../../libs/jquery-mousewheel.min.js');

import Vue from '../../libs/vue.min.js';
import VueRouter from 'vue-router';
import Clipboard from 'clipboard';

window.Clipboard = Clipboard;
window.Events = new Vue({});

window.utility = {
	isArray( data ) {
		return typeof data && Array.isArray(data);
	},

	isObject( data ) {
		return typeof data && !Array.isArray(data);
	}
}

window.GetUrl = function() {
	return '';
}

window.NonReactive = function(data) {
	return JSON.parse( JSON.stringify( data ) );
}

// Warn if overriding existing method
if(Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});

// global use
Vue.use(VueRouter);

const Welcome 			= require('../pages/welcome.vue');
const Showcase 			= require('../pages/showcase.vue');
const SingleShowcase 	= require('../pages/single-showcase.vue');
const Preferences 		= require('../pages/preferences.vue');
const Support 			= require('../pages/support.vue');
const Activation 		= require('../pages/activation.vue');
const AccessToken 		= require('../pages/access-token.vue');

Vue.component('input-tag', 			require('../components/input-tag.vue'));
Vue.component('input-increment', 	require('../components/input-increment.vue'));
Vue.component('input-range', 		require('../components/input-range.vue'));
Vue.component('input-checkbox', 	require('../components/input-checkbox.vue'));
Vue.component('input-radio', 		require('../components/input-radio.vue'));
Vue.component('input-select', 		require('../components/input-select/component/select.vue'));
Vue.component('input-color', 		require('../components/input-color.vue'));
Vue.component('input-toggle', 		require('../components/input-toggle.vue'));
Vue.component('editor-cm', 			require('../components/editor-codemirror/component/editor-cm.vue'));
Vue.component('add-wp-media', 		require('../components/add-wp-media.vue'));
Vue.component('draggable', 			require('vuedraggable'));


(function($){

	$(document).ready(function(){

		const routes = [
			{ path: '/', 							component: Welcome },
			{ path: '/showcase', 					component: Showcase },
			{ path: '/showcase/create', 			component: SingleShowcase },
			{ path: '/edit-showcase/:id', 			component: SingleShowcase },
			{ path: '/edit-showcase/:id/duplicate', component: SingleShowcase },
			{ path: '/preferences', 				component: Preferences },
			{ path: '/support', 					component: Support },
			{ path: '/activation', 					component: Activation },
			{ path: '/access-token', 				component: AccessToken },
			{ path: '/access-token/:code', 			component: AccessToken }
		];

		const router = new VueRouter({ routes });

		if( $('#wplogos-app').length > 0 ) {
			window.app = new Vue({
				router
			}).$mount('#wplogos-app');
		}

	});

})(jQuery);