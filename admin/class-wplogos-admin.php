<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://codebeck.com
 * @since      1.0.0
 *
 * @package    Wplogos
 * @subpackage Wplogos/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wplogos
 * @subpackage Wplogos/admin
 * @author     CodeBeck <codebeck@gmail.com>
 */
class Wplogos_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wplogos_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wplogos_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700', false ); 

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wplogos-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wplogos_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wplogos_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_media();

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wplogos-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function add_plugin_admin_menu() {

		add_menu_page(
			'Wp Logos - Best WordPress Logos Showcase Plugin',
			'Wp Logos',
			'manage_options',
			'wplogos',
			array( $this, 'wplogos_admin_view')
			// 'to/icon/file.svg',
			// '2.1'
		);

	}

	public function wplogos_admin_view() {
		require plugin_dir_path( __FILE__ ) . 'partials/wplogos-admin-display.php';
	}

}
