<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://coderpixel.com
 * @since      1.0.0
 *
 * @package    Wplogos
 * @subpackage Wplogos/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="app-container">
	<div class="main-container">
		
		<div id="wplogos-app">

			<header class="wplogos-header">
				<div class="container-fluid">
					<div class="row">
						<div class="logo-area col-sm-4">
							<router-link to="/"><img src="<?php echo plugin_dir_url( dirname(__FILE__) ); ?>/img/wplogos-logo.png" alt="Wplogos Logo"></router-link>
						</div>
						<div class="menu-area col-sm-8 text-right">
							<ul>
								<router-link to="/showcase" tag="li"><a>Showcase</a></router-link>
								<router-link to="/preferences" tag="li"><a>Preferences</a></router-link>
								<router-link to="/support" tag="li"><a>Support</a></router-link>
								<router-link to="/activation" tag="li" class="activation"><a>Activation</a></router-link>
							</ul>
						</div>
					</div>
				</div>
			</header>

			<div class="wplogos-app-view-container">
				<router-view></router-view>
			</div>


			<!-- <single-feed></single-feed> -->

		</div>
		
	</div>
</div>