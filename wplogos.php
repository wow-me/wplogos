<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://domain.com
 * @since             1.0.0
 * @package           Wplogos
 *
 * @wordpress-plugin
 * Plugin Name:       Wp Logos
 * Plugin URI:        https://domain.com/wplogos-wordpress-logos-showcase-multi-use-responsive-logos-plugin
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            CodeBeck
 * Author URI:        https://domain.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wplogos
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PLUGIN_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wplogos-activator.php
 */
function activate_wplogos() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wplogos-activator.php';
	Wplogos_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wplogos-deactivator.php
 */
function deactivate_wplogos() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wplogos-deactivator.php';
	Wplogos_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wplogos' );
register_deactivation_hook( __FILE__, 'deactivate_wplogos' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wplogos.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wplogos() {

	$plugin = new Wplogos();
	$plugin->run();

}
run_wplogos();
