module.exports = {
	plugins: {
		'postcss-import': {},
		'postcss-cssnext': {
			browsers: ['last 50 versions', '> 5%']
		},
		'cssnano': {
			zindex: false
		}
	}
}