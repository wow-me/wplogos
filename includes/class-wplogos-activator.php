<?php

/**
 * Fired during plugin activation
 *
 * @link       https://codebeck.com
 * @since      1.0.0
 *
 * @package    Wplogos
 * @subpackage Wplogos/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wplogos
 * @subpackage Wplogos/includes
 * @author     CodeBeck <codebeck@gmail.com>
 */
class Wplogos_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
