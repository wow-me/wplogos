<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://codebeck.com
 * @since      1.0.0
 *
 * @package    Wplogos
 * @subpackage Wplogos/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wplogos
 * @subpackage Wplogos/includes
 * @author     CodeBeck <codebeck@gmail.com>
 */
class Wplogos_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
