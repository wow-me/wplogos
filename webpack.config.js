const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSCSS = new ExtractTextPlugin({
    filename: (getPath) => {
        return getPath('[name]');
    },
    allChunks: true
});

module.exports = {
    entry: {
        'public/js/wplogos-public.js'     : "./dev/public/js/wplogos-public.js",
        'public/css/wplogos-public.css'   : "./dev/public/css/wplogos-public.scss",
        'admin/js/wplogos-admin.js'       : "./dev/admin/js/wplogos-admin.js",
        'admin/css/wplogos-admin.css'     : "./dev/admin/css/wplogos-admin.scss"
        // 'live/js/wplogos-live.js'         : "./dev/live/js/wplogos-live.js",
        // 'live/css/wplogos-live.css'       : "./dev/live/css/wplogos-live.scss"
    },
    output: {
        path: path.resolve(__dirname),
        filename: "[name]"
    },
    module: {
        rules: [{
            test: /\.js$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['es2015', { modules: false }]
                    ]
                }
            }]
        },{
            test: /\.scss$/,
            use: extractSCSS.extract({
                fallback: 'style-loader',
                use: [
                    { loader: 'css-loader', options: { importLoaders: 1, url: false } },
                    'postcss-loader',
                    'sass-loader'
                ]
            })
        },{
            test: /\.vue$/,
            use: [{
                loader: 'vue-loader',
                options: {
                    postLoaders: {
                        html: 'babel-loader'
                    },
                    loaders: {
                        js: 'babel-loader?presets[]=es2015'
                    }
                }
            }]
        }]
    },
    plugins: [
        extractSCSS,
        new webpack.ProvidePlugin({
            '$': 'jquery'
        }),
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'admin/js/bestinsta-admin.js',
        // }),
        // new webpack.optimize.UglifyJsPlugin({
        //     beautify: false,
        //     mangle: {
        //         screw_ie8: true,
        //         keep_fnames: true
        //     },
        //     compress: {
        //         screw_ie8: true,
        //         warnings: false
        //     },
        //     comments: false
        // })
    ],

    externals: {
        jquery: 'window.jQuery',
        wp: 'window.wp'
    }
}